<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;
use Illuminate\Support\Facades\Hash;



class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Crear un usuario
        $u = new user();
        $u->name = "Daniel";
        $u->email = "daniel@gmail.com";
        $u->password = Hash::make("1234");
        $u->save();

    }
}
